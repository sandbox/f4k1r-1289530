<?php
/**
 * Implementation of hook_blog_view()
 */

function advanced_blog_views_views_data() {
  $data['advanced_blog']['table']['group'] = t('Advanced Blog');
  $data['advanced_blog']['table']['base'] = array(
    'field' => 'uid', 
    'title' => t('Advanced Blog table'), 
    'help' => t('Maps to uid in user table.'), 
    'weight' => -10,
  );
  $data['advanced_blog']['table']['join'] = array(    
    'users' => array(
      'left_field' => 'uid', 
      'field' => 'uid',
    ),
  );
  // uid field
  $data['advanced_blog']['uid'] = array(
    'title' => t('User id'), 
    'help' => t('Unique id of user'),
    'relationship' => array(
      'base' => 'users', 
      'field' => 'uid', 
      'handler' => 'views_handler_relationship', 
      'label' => t('User uid'),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),    
  );
  // Blog Title field
  $data['advanced_blog']['title'] = array(
    'title' => t('Blog Title'), 
    'help' => t('Blog Title of user.'), 
    'field' => array(
      'handler' => 'views_handler_field', 
      'click sortable' => TRUE,
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ), 
  );
  // Blog Description field.
  $data['advanced_blog']['description'] = array(
    'title' => t('Blog Description'), 
    'help' => t('Blog Description of user.'), 
    'field' => array(
      'handler' => 'views_handler_field', 
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  return $data;
}
